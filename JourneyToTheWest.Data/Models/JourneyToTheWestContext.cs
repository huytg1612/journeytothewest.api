﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace JourneyToTheWest.Data.Models
{
    public partial class JourneyToTheWestContext : DbContext
    {
        public JourneyToTheWestContext()
        {
        }

        public JourneyToTheWestContext(DbContextOptions<JourneyToTheWestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Actor> Actor { get; set; }
        public virtual DbSet<Character> Character { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Scene> Scene { get; set; }
        public virtual DbSet<SceneActor> SceneActor { get; set; }
        public virtual DbSet<SceneCharacter> SceneCharacter { get; set; }
        public virtual DbSet<SceneTool> SceneTool { get; set; }
        public virtual DbSet<Tool> Tool { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=JourneyToTheWest;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Actor>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(11)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Character>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Scene>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FilmLocation).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.TimeEnd).HasColumnType("datetime");

                entity.Property(e => e.TimeStart).HasColumnType("datetime");
            });

            modelBuilder.Entity<SceneActor>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ActorId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CharacterId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SceneId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Actor)
                    .WithMany(p => p.SceneActor)
                    .HasForeignKey(d => d.ActorId)
                    .HasConstraintName("FK_SceneActor_Actor");

                entity.HasOne(d => d.Character)
                    .WithMany(p => p.SceneActor)
                    .HasForeignKey(d => d.CharacterId)
                    .HasConstraintName("FK_SceneActor_Character");

                entity.HasOne(d => d.Scene)
                    .WithMany(p => p.SceneActor)
                    .HasForeignKey(d => d.SceneId)
                    .HasConstraintName("FK_SceneActor_Scene");
            });

            modelBuilder.Entity<SceneCharacter>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CharacterId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SceneId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Character)
                    .WithMany(p => p.SceneCharacter)
                    .HasForeignKey(d => d.CharacterId)
                    .HasConstraintName("FK_SceneCharacter_Character");

                entity.HasOne(d => d.Scene)
                    .WithMany(p => p.SceneCharacter)
                    .HasForeignKey(d => d.SceneId)
                    .HasConstraintName("FK_SceneCharacter_Scene");
            });

            modelBuilder.Entity<SceneTool>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SceneId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeFrom).HasColumnType("datetime");

                entity.Property(e => e.TimeTo).HasColumnType("datetime");

                entity.Property(e => e.ToolId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Scene)
                    .WithMany(p => p.SceneTool)
                    .HasForeignKey(d => d.SceneId)
                    .HasConstraintName("FK_SceneTool_Scene");

                entity.HasOne(d => d.Tool)
                    .WithMany(p => p.SceneTool)
                    .HasForeignKey(d => d.ToolId)
                    .HasConstraintName("FK_SceneTool_Tool");
            });

            modelBuilder.Entity<Tool>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.User)
                    .HasForeignKey<User>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Actor");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Role");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
