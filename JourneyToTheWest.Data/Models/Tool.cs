﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.Data.Models
{
    public partial class Tool
    {
        public Tool()
        {
            SceneTool = new HashSet<SceneTool>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int? Amount { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<SceneTool> SceneTool { get; set; }
    }
}
