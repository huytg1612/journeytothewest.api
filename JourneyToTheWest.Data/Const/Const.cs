﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Const
{
    class Const
    {
    }

    public static class SystemRole
    {
        public const string ADMIN = "Admin";
        public const string ACTOR = "Actor";
    }
}
