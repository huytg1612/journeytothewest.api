﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IBaseRepository<E,K> where E : class
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        DbSet<E> Get();
        E GetById(K key);
        EntityEntry<E> Create(E entity);
        EntityEntry<E> Delete(E entity);

        EntityEntry<E> Update(E entity);
    }

    public abstract class BaseRepository<E, K> : IBaseRepository<E, K> where E : class
    {
        protected DbContext _context;
        DbSet<E> _set;

        public BaseRepository(DbContext context)
        {
            _context = context;
            _set = _context.Set<E>();
        }

        public EntityEntry<E> Create(E entity)
        {
            return _set.Add(entity);
        }

        public EntityEntry<E> Delete(E entity)
        {
            return _set.Remove(entity);
        }

        public DbSet<E> Get()
        {
            return _set;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public EntityEntry<E> Update(E entity)
        {
            return _context.Update(entity);
        }

        public abstract E GetById(K key);
    }
}
