﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface ISceneRepository : IBaseRepository<Scene, string>
    {
        Scene CreateScene(SceneCreateVMs model);
        Scene PrepareCreate(SceneCreateVMs model);
        Scene EditScene(Scene scene, SceneEditVMs model);
    }

    public  partial class SceneRepository : BaseRepository<Scene,string>, ISceneRepository
    {
        public SceneRepository(DbContext context) : base(context)
        {

        }

        public Scene CreateScene(SceneCreateVMs model)
        {
            return PrepareCreate(model);
        }

        public Scene EditScene(Scene scene, SceneEditVMs model)
        {
            scene.Name = model.Name;
            scene.Description = model.Description;
            scene.FilmLocation = model.FilmLocation;
            scene.Shot = model.Shot;
            scene.TimeStart = model.TimeStart;
            scene.TimeEnd = model.TimeEnd;

            return Update(scene).Entity;
        }

        public override Scene GetById(string key)
        {
            return Get().FirstOrDefault(s => s.Id == key);
        }

        public Scene PrepareCreate(SceneCreateVMs model)
        {
            var scene = new Scene
            {
                Id = Guid.NewGuid().ToString(),
                Description = model.Description,
                FilmLocation = model.FilmLocation,
                Name = model.Name,
                Shot = model.Shot,
                TimeEnd = model.TimeEnd,
                TimeStart = model.TimeStart
            };

            return Create(scene).Entity;
        }
    }
}
