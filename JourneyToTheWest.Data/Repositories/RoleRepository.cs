﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IRoleRepository : IBaseRepository<Role, string>
    {
        Role CreateRole(RoleCreateVMs model);
        Role PrepareCreate(RoleCreateVMs model);
    }

    public partial class RoleRepository : BaseRepository<Role, string>, IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {

        }

        public Role CreateRole(RoleCreateVMs model)
        {
            return PrepareCreate(model);
        }

        public override Role GetById(string key)
        {
            return Get().FirstOrDefault(r => r.Id == key);
        }

        public Role PrepareCreate(RoleCreateVMs model)
        {
            Role role = new Role
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
            };

            return Create(role).Entity;
        }
    }
}
