﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IActorRepository : IBaseRepository<Actor,string>
    {
        Actor CreateActor(ActorCreateVMs model);
        Actor PrepareCreate(ActorCreateVMs model);

        Actor EditActor(Actor actor, ActorEditVMs model);
    }

    public partial class ActorRepository : BaseRepository<Actor,string>, IActorRepository
    {
        public ActorRepository(DbContext context) : base(context)
        {

        }

        public Actor CreateActor(ActorCreateVMs model)
        {
            return PrepareCreate(model);
        }

        public Actor PrepareCreate(ActorCreateVMs model)
        {
            var actor = new Actor
            {
                Id = model.Id,
                Description = model.Description,
                Email = model.Email,
                Image = model.Image,
                Name = model.Name
            };

            return Create(actor).Entity;
        }

        public override Actor GetById(string key)
        {
            return Get().FirstOrDefault(a => a.Id == key);
        }

        public Actor EditActor(Actor actor, ActorEditVMs model)
        {

            actor.Name = model.Name;
            actor.Description = model.Description;
            actor.Email = model.Email;
            actor.Image = model.Image;
            actor.Phone = model.Phone;

            return Update(actor).Entity;
        }
    }
}
