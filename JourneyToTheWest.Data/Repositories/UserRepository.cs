﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JourneyToTheWest.Data.Repositories
{
    public partial interface IUserRepository : IBaseRepository<User, string>
    {
        User CreateUser(UserCreateVMs model);
        User PrepareCreate(UserCreateVMs model);
    }

    public partial class UserRepository : BaseRepository<User, string>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {

        }

        public User CreateUser(UserCreateVMs model)
        {
            return PrepareCreate(model);
        }

        public override User GetById(string key)
        {
            return Get().FirstOrDefault(u => u.Id == key);
        }

        public User PrepareCreate(UserCreateVMs model)
        {
            var user = new User
            {
                Id = Guid.NewGuid().ToString(),
                Password = model.Password,
                RoleId = model.RoleId,
                Username = model.Username
            };

            return Create(user).Entity;
        }
    }
}
