﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Globals
{
    public static class Global
    {
        public static void ConfigIoC(IServiceCollection services)
        {
            services.AddScoped<DbContext, JourneyToTheWestContext>()
                    .AddScoped<IRoleRepository, RoleRepository>()
                    .AddScoped<IUserRepository, UserRepository>()
                    .AddScoped<IActorRepository, ActorRepository>()
                    .AddScoped<ISceneRepository, SceneRepository>()
                    .AddScoped<HumanResourceDomain>()
                    .AddScoped<AuthorizeDomain>()
                    .AddScoped<StudioDomain>();
        }
    }
}
