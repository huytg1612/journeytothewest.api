﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.ViewModels
{
    public class SceneVMs
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? TimeStart { get; set; }
        public DateTime? TimeEnd { get; set; }
        public int? Shot { get; set; }
        public string FilmLocation { get; set; }
    }

    public class SceneCreateVMs
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("time_start")]
        public DateTime? TimeStart { get; set; }
        [JsonProperty("time_end")]
        public DateTime? TimeEnd { get; set; }
        [JsonProperty("shot")]
        public int? Shot { get; set; }
        [JsonProperty("film_location")]
        public string FilmLocation { get; set; }
    }

    public class SceneEditVMs
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("time_start")]
        public DateTime? TimeStart { get; set; }
        [JsonProperty("time_end")]
        public DateTime? TimeEnd { get; set; }
        [JsonProperty("shot")]
        public int? Shot { get; set; }
        [JsonProperty("film_location")]
        public string FilmLocation { get; set; }
    }
}
