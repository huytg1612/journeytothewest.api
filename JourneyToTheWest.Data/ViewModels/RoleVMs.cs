﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.ViewModels
{
    public class RoleVMs
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleCreateVMs
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
