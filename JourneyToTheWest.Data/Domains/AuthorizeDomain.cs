﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public class AuthorizeDomain
    {
        IUserRepository _userRepo;

        public AuthorizeDomain(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public string GenerateToken(UserVMs model)
        {
            var handler = new JwtSecurityTokenHandler();
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, model.Username));
            claims.Add(new Claim("Id", model.Id));
            claims.Add(new Claim("RoleId", model.RoleId));

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);
            var key = Encoding.ASCII.GetBytes("JNTTW_Token_Secret");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claimsIdentity,
                Issuer = "JNTTW_Issuer",
                Audience = "JNTTW_Audience",
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var newToken = handler.CreateToken(tokenDescriptor);
            return handler.WriteToken(newToken);
        }

        public User GetSpecificUserDetail(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var result = handler.ReadJwtToken(token) as JwtSecurityToken;
            var currentClaims = result.Claims.ToList();

            string id = currentClaims.FirstOrDefault(t => t.Type == "Id").Value;
            string roleId = currentClaims.FirstOrDefault(t => t.Type == "RoleId").Value;
            var user = _userRepo.Get().FirstOrDefault(u => u.Id == id);

            return user;
        }

        public UserVMs GetUserDetail(string token)
        {
            var user = GetSpecificUserDetail(token);
            if (user != null)
            {
                var userVMs = new UserVMs
                {
                    Id = user.Id,
                    Username = user.Username,
                    RoleId = user.RoleId
                };

                return userVMs;
            }

            return null;
        }

        public UserVMs Login(UserLoginVMs model)
        {
            var user = _userRepo.Get().FirstOrDefault(u => u.Username == model.Username && u.Password == model.Password);
            if(user != null)
            {
                var userVMs = new UserVMs
                {
                    Id = user.Id,
                    Username = user.Username,
                    Password = user.Password,
                    RoleId = user.RoleId
                };

                return userVMs;
            }

            return null;
        }
    }
}
