﻿using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public class StudioDomain
    {
        ISceneRepository _sceneRepo;

        public StudioDomain(ISceneRepository sceneRepo)
        {
            _sceneRepo = sceneRepo;
        }

        public Scene CreateScene(SceneCreateVMs model)
        {
            var scene = _sceneRepo.CreateScene(model);
            _sceneRepo.SaveChanges();
            return scene;
        }

        public Scene GetSceneById(string id)
        {
            return _sceneRepo.GetById(id);
        }

        public bool EditScene(string id,SceneEditVMs model)
        {
            var scene = _sceneRepo.GetById(id);
            _sceneRepo.EditScene(scene, model);
            _sceneRepo.SaveChanges();

            return true;
        }
    }
}
