﻿using JourneyToTheWest.Data.Const;
using JourneyToTheWest.Data.Models;
using JourneyToTheWest.Data.Repositories;
using JourneyToTheWest.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace JourneyToTheWest.Data.Domains
{
    public class HumanResourceDomain
    {
        IRoleRepository _roleRepo;
        IUserRepository _userRepo;
        IActorRepository _actorRepo;

        public HumanResourceDomain(IRoleRepository roleRepo, IUserRepository userRepo, IActorRepository actorRepo)
        {
            _roleRepo = roleRepo;
            _userRepo = userRepo;
            _actorRepo = actorRepo;
        }

        public Role CreateRole(RoleCreateVMs model)
        {
            var role = _roleRepo.CreateRole(model);
            _roleRepo.SaveChanges();
            return role;
        }

        public IQueryable GetRole()
        {
            return _roleRepo.Get();
        }

        public IQueryable GetUser()
        {
            return _userRepo.Get();
        }

        public User CreateUser(UserCreateVMs model)
        {
            var user = _userRepo.CreateUser(model);
            _userRepo.SaveChanges();
            return user;
        }

        public Actor CreateActor(ActorCreateVMs model)
        {
            var user = _userRepo.GetById(model.Id);
            if(user != null)
            {
                var role = _roleRepo.GetById(user.RoleId);
                if(role != null)
                {
                    if(role.Name.ToLower() == SystemRole.ACTOR.ToLower())
                    {
                        var actor = _actorRepo.CreateActor(model);
                        _actorRepo.SaveChanges();
                        return actor;
                    }
                }
            }

            return null;
        }

        public bool EditActor(string id, ActorEditVMs model)
        {
            var actor = _actorRepo.GetById(id);
            _actorRepo.EditActor(actor, model);
            _actorRepo.SaveChanges();

            return true;
        }

        public Actor GetActorById(string id)
        {
            var actor = _actorRepo.GetById(id);
            return actor;
        }

    }
}
