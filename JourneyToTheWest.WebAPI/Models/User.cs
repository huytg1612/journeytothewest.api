﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.WebAPI.Models
{
    public partial class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual Actor Actor { get; set; }
    }
}
