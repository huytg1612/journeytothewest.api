﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.WebAPI.Models
{
    public partial class Scene
    {
        public Scene()
        {
            SceneActor = new HashSet<SceneActor>();
            SceneCharacter = new HashSet<SceneCharacter>();
            SceneTool = new HashSet<SceneTool>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? TimeStart { get; set; }
        public DateTime? TimeEnd { get; set; }
        public int? Shot { get; set; }
        public string FilmLocation { get; set; }

        public virtual ICollection<SceneActor> SceneActor { get; set; }
        public virtual ICollection<SceneCharacter> SceneCharacter { get; set; }
        public virtual ICollection<SceneTool> SceneTool { get; set; }
    }
}
