﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.WebAPI.Models
{
    public partial class SceneTool
    {
        public string Id { get; set; }
        public string ToolId { get; set; }
        public string SceneId { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }

        public virtual Scene Scene { get; set; }
        public virtual Tool Tool { get; set; }
    }
}
