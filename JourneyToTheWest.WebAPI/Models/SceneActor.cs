﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.WebAPI.Models
{
    public partial class SceneActor
    {
        public string Id { get; set; }
        public string SceneId { get; set; }
        public string ActorId { get; set; }
        public string CharacterId { get; set; }
        public string Description { get; set; }

        public virtual Actor Actor { get; set; }
        public virtual Character Character { get; set; }
        public virtual Scene Scene { get; set; }
    }
}
