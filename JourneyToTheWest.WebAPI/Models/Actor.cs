﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.WebAPI.Models
{
    public partial class Actor
    {
        public Actor()
        {
            SceneActor = new HashSet<SceneActor>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Image { get; set; }

        public virtual User IdNavigation { get; set; }
        public virtual ICollection<SceneActor> SceneActor { get; set; }
    }
}
