﻿using System;
using System.Collections.Generic;

namespace JourneyToTheWest.WebAPI.Models
{
    public partial class Character
    {
        public Character()
        {
            SceneActor = new HashSet<SceneActor>();
            SceneCharacter = new HashSet<SceneCharacter>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SceneActor> SceneActor { get; set; }
        public virtual ICollection<SceneCharacter> SceneCharacter { get; set; }
    }
}
