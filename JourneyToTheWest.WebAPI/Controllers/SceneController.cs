﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebAPI.Controllers
{
    [Route("api/scene")]
    [ApiController]
    public class SceneController : ControllerBase
    {
        StudioDomain _studioDomain;

        public SceneController(StudioDomain studioDomain)
        {
            _studioDomain = studioDomain;
        }

        [HttpPost]
        public IActionResult CreateScene([FromBody] SceneCreateVMs model)
        {
            try
            {
                var scene = _studioDomain.CreateScene(model);
                if(scene != null)
                {
                    return Ok(scene);
                }

                return BadRequest("Create failed");
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("edit/{id}")]
        public IActionResult EditScene(string id, [FromBody] SceneEditVMs model)
        {
            try
            {
                var isEdited = _studioDomain.EditScene(id, model);
                if (isEdited)
                {
                    return Ok("Edit successful");
                }

                return BadRequest("Edit failed");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            try
            {
                var scene = _studioDomain.GetSceneById(id);
                return Ok(scene);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
