﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebAPI.Controllers
{
    [Route("api/role")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        HumanResourceDomain _hrDomain;

        public RoleController(HumanResourceDomain hrDomain)
        {
            _hrDomain = hrDomain;
        }

        [HttpGet]
        public IActionResult GetRole()
        {
            try
            {
                var listRole = _hrDomain.GetRole();
                return Ok(listRole);
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public IActionResult CreateRole([FromBody] RoleCreateVMs model)
        {
            try
            {
                var role = _hrDomain.CreateRole(model);
                if(role != null)
                {
                    return Ok(role.Id);
                }

                return BadRequest();
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
