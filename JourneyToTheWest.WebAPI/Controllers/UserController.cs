﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebAPI.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        HumanResourceDomain _hrDomain;
        AuthorizeDomain _authDomain;

        public UserController(HumanResourceDomain hrDomain, AuthorizeDomain authDomain)
        {
            _hrDomain = hrDomain;
            _authDomain = authDomain;
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] UserCreateVMs model)
        {
            try
            {
                var user = _hrDomain.CreateUser(model);
                if(user != null)
                {
                    return Ok(user.Id);
                }

                return BadRequest();
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] UserLoginVMs model)
        {
            try
            {
                var userVMs = _authDomain.Login(model);

                if(userVMs != null)
                {
                    var userToken = _authDomain.GenerateToken(userVMs);
                    return Ok(userToken);
                }

                return BadRequest("Wrong username or password");
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IActionResult GetUserDetail()
        {
            try
            {
                string token = Request.Headers["Authorization"].ToString().Split(" ")[1];
                var result = _authDomain.GetUserDetail(token);
                if(result != null)
                {
                    return Ok(result);
                }

                return BadRequest("Token is not exist");
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
