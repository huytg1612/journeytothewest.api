﻿using JourneyToTheWest.Data.Domains;
using JourneyToTheWest.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JourneyToTheWest.WebAPI.Controllers
{
    [Route("api/actor")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        HumanResourceDomain _humanDomain;

        public ActorController(HumanResourceDomain humanDomain)
        {
            _humanDomain = humanDomain;
        }

        [HttpPost]
        public IActionResult CreateActor([FromBody] ActorCreateVMs model)
        {
            try
            {
                var actor = _humanDomain.CreateActor(model);
                if(actor != null)
                {
                    return Ok(actor);
                }

                return BadRequest("Something went wrong");
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetActorById(string id)
        {
            try
            {
                var actor = _humanDomain.GetActorById(id);
                if (actor != null)
                {
                    return Ok(actor);
                }

                return BadRequest("Something went wrong");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("edit/{id}")]
        public IActionResult EditActor(string id, [FromBody] ActorEditVMs model)
        {
            try
            {
                var isEdited = _humanDomain.EditActor(id, model);
                if(isEdited)
                {
                    return Ok("Edit successful");
                }

                return BadRequest("Something went wrong");
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
